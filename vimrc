""""""""""MY SETTINGS""""""""""""""
" General settings
set number " show line numbers
set showcmd " echo letters as we type commands out
set showmatch " highlight the matching brace

" Custom commands
command! Test echo "Hello!"

" Highlight trailing whitespaces
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/

" Tab settings
set expandtab " enable replacement of tabs with spaces
set tabstop=16 " show existing tab with n spaces width
set softtabstop=4 " when we insert new tab characters, set it to n spaces
set shiftwidth=4 " when indenting with >>, use n spaces width
" set autoindent

" search settings
set ignorecase " ignore cases when doing search
set smartcase " if search has a capital, do the search case sensitively
set incsearch " highlight words as you type in the search

" highlight chars that are 80+ columns
highlight ColorColumn ctermbg=magenta
call matchadd('ColorColumn', '\%80v', 100)

" apply indentation and plugin for files based on file types
filetype indent on
filetype plugin indent on


" register skeleton files
source ~/.vim/custom/file_template_autocommands.vim
