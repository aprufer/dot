alias cmake="cmake -B build -S . "
alias lt='ls --human-readable --size -1 -S --classify'
alias black="black --line-length 79 --target-version py38"
alias memcheck="valgrind --tool=memcheck --leak-check=full --show-leak-kinds=all --track-fds=yes"
alias helgrind="valgrind --tool=helgrind"
alias drd="valgrind --tool=drd"

