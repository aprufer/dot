#include <CUnit/Basic.h>
#include <CUnit/CUnit.h>

static int setup_suite(void)
{
    return 0;
}

static int teardown_suite(void)
{
    return 0;
}

static void setup_test(void)
{
}

static void teardown_test(void)
{
}

static void test_abc(void)
{
    CU_ASSERT(1 == 0);
}

static CU_TestInfo tests[] = {

    {"test_abc():", test_abc},

    CU_TEST_INFO_NULL};

CU_SuiteInfo abc_test_suite[]
    = {{"Suite abc Tests:", setup_suite, teardown_suite, setup_test,
        teardown_test, tests},
       CU_SUITE_INFO_NULL};

/*** END OF FILE ***/

