cmake_minimum_required(VERSION 3.16)

set(TARGET_NAME a.out)

set(SRC_DIR src)

set(SRC_FILES
    ${SRC_DIR}/hello.c
)

project(${TARGET_NAME})

foreach(SRC_FILE ${SRC_FILES})
    add_executable(${TARGET_NAME} ${SRC_FILE})
endforeach(SRC_FILE)
