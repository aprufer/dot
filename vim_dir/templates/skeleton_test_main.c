#include <CUnit/Basic.h>
#include <CUnit/CUnit.h>

int main(void)
{
    extern CU_SuiteInfo test_suite_abc;

    CU_SuiteInfo suites[]
        = {test_suite_abc, CU_SUITE_INFO_NULL};

    if (0 != CU_initialize_registry())
    {
        fprintf(stderr, "CUnit Error: %s\n", CU_get_error_msg());
        return CU_get_error();
    }

    if (0 != CU_register_suites(suites))
    {
        fprintf(stderr, "CUnit Error: %s\n", CU_get_error_msg());
        CU_cleanup_registry();
        return CU_get_error();
    }

    // set cunit test mode to verbose and run it
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();

    // handle output and cleanup
    CU_basic_show_failures(CU_get_failure_list());
    unsigned int num_failed = CU_get_number_of_failures();

    CU_cleanup_registry(); // clean up cunit memory leaks
    printf("\n");
    return (int)num_failed;
}
/*** END OF FILE ***/

