function! ExcludePatterns(patterns, file)
  for pattern in a:patterns
    if a:file =~# '^' . pattern
      return 1
    endif
  endfor
  return 0
endfunction

let g:exclude_patterns_c = ['test',]
let g:exclude_patterns_python = ['Test',]

if has("autocmd")
  augroup templates
    " Python templates"
    autocmd BufNewFile *.py silent! if !ExcludePatterns(g:exclude_patterns_python, expand('%:t')) | 0r ~/.vim/templates/skeleton.py | endif
    autocmd BufNewFile *.py silent! if !ExcludePatterns(g:exclude_patterns_python, expand('%:t')) | execute 'normal! Gdd' | endif

    "C source code and header templates"
    " Use the skeleton.c file if file does not begin with any strings in exclude_patterns_c
    autocmd BufNewFile *.c silent! if !ExcludePatterns(g:exclude_patterns_c, expand('%:t')) | 0r ~/.vim/templates/skeleton.c | endif
    autocmd BufNewFile *.c silent! if !ExcludePatterns(g:exclude_patterns_c, expand('%:t')) | let b:filename = substitute(expand('%'), '\.h', '', '') | endif
    autocmd BufNewFile *.c silent! if !ExcludePatterns(g:exclude_patterns_c, expand('%:t')) | let b:guardname = substitute(expand('%:t:r'), '\W', '_', 'g') . '.h' | endif
    autocmd BufNewFile *.c silent! if !ExcludePatterns(g:exclude_patterns_c, expand('%:t')) | execute '%s/HEADER_FILE/\' . b:guardname . '/g' | endif
    autocmd BufNewFile *.c silent! if !ExcludePatterns(g:exclude_patterns_c, expand('%:t')) | execute 'normal! Gdd' | endif

    autocmd BufNewFile *.h 0r ~/.vim/templates/skeleton.h
    autocmd BufNewFile *.h :let b:filename = substitute(expand('%'), '\.h', '', '')
    autocmd BufNewFile *.h :let b:guardname = substitute(toupper(expand('%:t:r')), '\W', '_', 'g') . '_H'
    autocmd BufNewFile *.h :execute '%s/_HEADER_GUARD/\' . b:guardname . '/g'
    autocmd BufNewFile *.h execute 'normal! Gdd'

    autocmd BufNewFile Makefile 0r ~/.vim/templates/Makefile
    autocmd BufNewFile CMakeLists.txt 0r ~/.vim/templates/CMakeLists.txt

    " Handle special cases for test_main.c and other test*.c files
    autocmd BufNewFile test_main.c 0r ~/.vim/templates/skeleton_test_main.c
    autocmd BufNewFile test_main.c execute 'normal! Gdd'

    " Load the skeleton_test_abc.c template for test*.c files, excluding test_main.c
    autocmd BufNewFile test*.c silent! if expand('%:t') !=# 'test_main.c' | 0r ~/.vim/templates/skeleton_test_abc.c | endif
    autocmd BufNewFile test*.c silent! if expand('%:t') !=# 'test_main.c' | execute 'normal! Gdd' | endif

  augroup END
endif

