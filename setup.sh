#!/usr/bin/env bash

# Ref: https://stackoverflow.com/a/246128
script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

config_dir=$HOME/.config

# Given 1) a filename with the path, 2) a symlink target,
# make a backup of the filename if it already exists
# replace filename with the symlink target path
relink_file() {
    ln_target=$1; shift
    ln_link_name=$1; shift
    echo "DEBUG: ln_link_name = $ln_link_name"
    echo "DEBUG: ln_target = $ln_target"

    # Ensure ln_target exists (so that we don't create a broken symlink)
    if [[ ! -e $ln_target ]]; then
        >&2 echo "ERROR: $ln_target does not exist"
        return 1
    fi

    # If the ln_link_name is already a symlink to ln_target, return
    if [[ $(readlink -f $ln_link_name) == $ln_target ]]; then
        echo "INFO: $ln_link_name already points to $ln_target"
        return 2
    fi

    # make the symlink
    echo -n "INFO: creating link.. "
    ln --verbose -s --backup=numbered $ln_target $ln_link_name
}

# Link the hidden rc files in $HOME to the templates in this git repo
relink_rc_files() {
    for file in $@;
	do
        relink_file $script_dir/$file $HOME/.$file
    done
}

# Link files located in the ~/.config/whatever
relink_config_files() {

    # Git
    mkdir -p $config_dir/git

    relink_file $script_dir/git_config $config_dir/git/config
    relink_file $script_dir/git_ignore $config_dir/git/ignore

    # VSCode
    mkdir -p $config_dir/Code/User
    relink_file $script_dir/vs_code/vs_code_settings $config_dir/Code/User/settings.json
    ln --verbose -s --backup=numbered $script_dir/vs_code/snippets $config_dir/Code/User/snippets

}

relink_misc() {

    # ~/.vim
    if [[ -d $HOME/.vim ]]; then
        mv $HOME/.vim $HOME/.vim.bak
    fi

    relink_file $script_dir/vim_dir $HOME/.vim


}


#****** MAIN ******#


relink_rc_files "bashrc" "vimrc" "tmux.conf" "tmux.conf.local" "zshrc" "bash_aliases"

# Link the rest of the files that are scattered throughout ~/.config
mkdir -p $config_dir # -p to quiet error if it exists

# Create directories and config files inside of ~/.config
relink_config_files

relink_misc

git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim
