#!/usr/bin/env python3

import argparse
from typing import Any

def parse_command_line(program_description: str) -> Any:
    """
    Parse the command line arguments for the program.

    Args:
        program_description (str): The program description.

    Returns:
        argparse.Namespace: The parsed arguments.
    """
    parser = argparse.ArgumentParser(
        description=program_description,
        prog="client.py",
        usage="./%(prog)s [-t --TITLE] [-b --BOX] [-s --STAR] [-h --HELP]",
    )

    # Title
    parser.add_argument(
        "-t",
        "--TITLE",
        required=True,
        help="Name of the C section header to generate",
    )

    # Box flag
    parser.add_argument(
        "-b",
        "--BOX",
        action="store_true",
        help="Use a boxed design for the section header",
    )

    # Star flag
    parser.add_argument(
        "-s",
        "--STAR",
        action="store_true",
        help="Use a starred design for the section header",
    )

    args = parser.parse_args()
    return args

# Function to generate a section header for a C file
def generate_c_section_header(title, use_box, use_star):
    # The total width for the section header is 79 characters, but 2 are used for "//"
    total_width = 77
    
    # The title should be uppercase and centered
    title = title.upper()

    if use_star:
        # Generate a starred section header
        section_header = f"// {'*' * (total_width)}\n// {title.center(total_width)}\n// {'*' * (total_width)}"
    elif use_box:
        # Generate a boxed section header
        section_header = f"// +{'-' * (total_width - 2)}+\n// | {title.center(total_width - 4)} |\n// +{'-' * (total_width - 2)}+"
    else:
        # Calculate the number of dashes required on each side of the title
        num_dashes_each_side = (total_width - len(title) - 2) // 2  # Subtract 2 for the spaces next to the title
        
        # Generate the dashes for each side
        dashes_each_side = '-' * num_dashes_each_side
        
        # If the title + dashes + 2 spaces is less than 79, add an additional dash to the right side
        if len(title) + (num_dashes_each_side * 2) + 2 < total_width:
            dashes_each_side += '-'
        
        # Assemble the full section header
        section_header = f"//\n// {dashes_each_side}{title}{dashes_each_side}\n//"
    
    return section_header

def main():
    args = parse_command_line("C Section Header Generator")
    header = generate_c_section_header(args.TITLE, args.BOX, args.STAR)
    print(header)

if __name__ == "__main__":
    main()