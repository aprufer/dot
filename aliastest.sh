#!/usr/bin/env bash

# Run with bash -i aliastest.sh ll
# Ref: https://unix.stackexchange.com/a/732235

is_alias() {
    if [[ "$(type -t $1)" == "alias" ]]; then
        echo "$1 is an alias"
    else
        echo "Cannot find $1"
    fi
}

# == MAIN ==

if [[ $# -lt 1 ]]; then
    echo "Usage: $0 <command> [ <command> ...]"
    exit 1
fi

for cmd in $@; do
    is_alias $cmd
done
